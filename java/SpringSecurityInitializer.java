package org.metropolis.company.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.web.context.AbstractSecurityWebApplicationInitializer;

/**
 * Created by metropolis on 2/28/2017.
 */
@Configuration
public class SpringSecurityInitializer extends AbstractSecurityWebApplicationInitializer {

}
